package test;

public class CalcFunction {
	    private String result;
	
	    public void calc(String s){
	    	if(s.indexOf("+")>-1){
	    		int i=s.indexOf("+");
	    		if(s.indexOf("+",i+1)==i+1){
	    			throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
	    			}else{
	    				add(s);
	    				}
	    		}
	        if(s.indexOf("-")>-1){
		        	int i=s.indexOf("-");            
		            if(s.indexOf("-",i+1)==i+1){           
		            	throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
		               }else{
		            	   substract(s);
		               }
		        
	        }
	        if(s.indexOf("*")>-1){
		        	int i=s.indexOf("*");            
		            if(s.indexOf("*",i+1)==i+1){           
		            	throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
		               }else{
		            	   multiply(s);
		               }     
	        }
	        if(s.indexOf("÷")>-1){
		        	int i=s.indexOf("÷");            
		            if(s.indexOf("÷",i+1)==i+1){           
		            	throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
		               }else{
		            	   divide(s);
		               } 
	        }
	        System.out.println(getResult());
	    }
	    
	    
	    public void add(String s)//加法
	    {
	        String[] str=s.split("[+]");
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=str[0].split("[/]");
	        	String[] str2=str[1].split("[/]");
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//分母不为零
	        	{
	        		result =simplefraction(((Integer.parseInt(str1[0])*Integer.parseInt(str1[1]))+(Integer.parseInt(str2[0])*Integer.parseInt(str1[1]))),(Integer.parseInt(str1[1])*Integer.parseInt(str2[1])));      	
	        	}else{
	        		throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	        }
	        else{//整数
	 	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
	            {	           
	 	    	   result = Integer.parseInt(str[0])+Integer.parseInt(str[1])+""; 
	 	    	   }       	
	        
	       else{
	    	   throw new IllegalArgumentException("overrun!");}//数值范围超出时抛出异常
	       } 
	        
	    }
	    
	    public void substract(String s)//减法
	    {
	        String[] str=s.split("[-]");
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=str[0].split("[/]");
	        	String[] str2=str[1].split("[/]");
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//分母不为零
	        	{
	        		result =simplefraction(((Integer.parseInt(str1[0])*Integer.parseInt(str2[1]))-(Integer.parseInt(str2[0])*Integer.parseInt(str1[1]))),(Integer.parseInt(str1[1])*Integer.parseInt(str2[1])));    	
	        	}else{
	        		throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	        }
	        else{//整数
	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
	            {
	    	   result = Integer.parseInt(str[0])-Integer.parseInt(str[1])+"";
	    	   }       	
	        
	       else{
	    	   throw new IllegalArgumentException("overrun!");}//数值范围超出时抛出异常
	       }
	    }
	    
	    
	    public void multiply(String s)//乘法
	    {
	        String[] str=s.split("[*]");
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=str[0].split("[/]");
	        	String[] str2=str[1].split("[/]");
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//
	        	{
	            	result =simplefraction(Integer.parseInt(str1[0])*Integer.parseInt(str2[0]),Integer.parseInt(str1[1])*Integer.parseInt(str2[1]));    	
	        	}else{
	        		throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	  
	        }
	        else{//整数
		 	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
		            {	            result = Integer.parseInt(str[0])*Integer.parseInt(str[1])+"";   }       	
		        
		       else{
		    	   throw new IllegalArgumentException("overrun!");}//数值范围超出时抛出异常
	        	
	        	}
	    }
	    
	    
	    public void divide(String s)//除法
	    {
	        String[] str=s.split("[÷]");
	        
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=str[0].split("[/]");
	        	String[] str2=str[1].split("[/]");
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//分母不为零
	        	{
	            	result =simplefraction(Integer.parseInt(str1[0])*Integer.parseInt(str2[1]),Integer.parseInt(str1[1])*Integer.parseInt(str2[0]));  	
	        	}else{
	        		throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	  
	        }else{
	        	if(Integer.parseInt(str[1]) != 0){//整数
			 	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
			            {	            
			 	    	   result = Integer.parseInt(str[0])/Integer.parseInt(str[1])+"";
			            }       	
			        
			       else{
			    	   throw new IllegalArgumentException("overrun!");}
	                         		
	        	}else {
	        		
	        		throw new IllegalArgumentException("Divisor cannot be zero!");   //除数为零时抛出异常  	
	        	}
	        }
	    }
    
    public String getResult()
    {
        return result;
    } 
	    
	
    public static int GCD(int m, int n) {//递归法求最大公约数
        if(m % n == 0){
            return n;
        }else{
            while(m != n){
                if(m > n)m = m - n;
                else n = n - m;
            }
        }
        return m;
}
    public static int LCM(int m, int n){//求最小公倍数
        return m * n / GCD(m,n);
    }
    
    static String simplefraction(int m, int n){ //化简分数
    String s;
    int num = GCD(m, n);
            m = m / num;
            n = n / num;
    if (n == 1) {
        s = m + "";
    } else {
        s = m + "" + "/" + n + "";
    }
    return s;
}
	
}