package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalcFunctionTest {

    CalcFunction calc = new CalcFunction();
    @Before
    public void setUp() throws Exception {
    }
    @Test
    public void testCalc() {
    	
    }
    @Test
    public void testAdd() {
        calc.add("1+2");
        assertEquals("3" , calc.getResult());
        calc.add("2/3+2/3");
        assertEquals("4/3" , calc.getResult());

    }
    @Test
    public void testSubstract() {
        calc.substract("3-1");
        assertEquals("2", calc.getResult());
        calc.substract("2/3-1/3");
        assertEquals("1/3", calc.getResult());

    }
    @Test
    public void testMultiply() {
        calc.multiply("2*3");
        assertEquals("6" , calc.getResult());
        calc.multiply("2/3*1/3");
        assertEquals("2/9" , calc.getResult());
    }
    @Test
    public void testDivide() {
        calc.divide("8��4");
        assertEquals("2", calc.getResult());
        calc.divide("1/4��1/4");
        assertEquals("1", calc.getResult());
    }

}
